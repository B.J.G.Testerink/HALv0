package org.hal.helloworld;

import org.hal.core.agent.Context;

public final class HelloWorldContext implements Context {
	private final String greeting;
	
	public HelloWorldContext(final String greeting) {
		this.greeting = greeting;
	}
	
	public final void greet(final String name) {
		System.out.println(this.greeting + " " + name + "!");
	}
}
