package org.hal.helloworld;

import org.hal.core.agent.Agent;
import org.hal.core.agent.AgentSpecification; 

/**
 * This applications demonstrates a basic agent with a context. 
 */
public final class HelloWorld {
    public final static void main(final String[] args){
    	
    	// The agent program specification
    	final AgentSpecification specification = 
    			(new AgentSpecification())
    			// Add the context supplier
    			.addContext(() -> new HelloWorldContext("Hello"))
    			// Add the plan scheme for processing HelloTriggers
    			.addExternalPlanScheme(HelloTrigger.class, (trigger, controller) -> {
    				// Retrieve the context and greet the given name
    				controller
    					.getContext(HelloWorldContext.class)
    					.greet(trigger.getName());
    				// Kill the agent to end the demonstration
    				controller.kill(); 
    			});
    	
    	// Create the agent using the specification
        final Agent agent = Agent.create(specification);

        // Send it a trigger to greet the world 
    	agent.add(new HelloTrigger("World"));
    }
}
