package org.hal.helloworld;

import org.hal.core.agent.Trigger;

public class HelloTrigger implements Trigger {
	private final String name;
	
	public HelloTrigger(final String name) { 
		this.name = name;
	}
	
	public final String getName() {
		return this.name;
	}
}
