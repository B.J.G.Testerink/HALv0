package org.hal.core.agent;
 
import java.util.Map.Entry;
import java.util.function.Consumer;
/**
 * This class provides an interface to an agent. To specify an agent, program an <code>AgentSpecification</code>. 
 * The create methods of this class can be used to create independent agents or add an agent to an existing platform. 
 * Note that running agents independently can be CPU heavy if the number of agents increases. 
 */
public final class Agent {
	/** The agent that is exposed by this interface. */
	private final AgentState state;
	
	/** Function to call when the agent is killed. */
	private final Runnable killFunction;
	
	Agent(final AgentState state, final Runnable killFunction){
		this.state = state;
		this.killFunction = killFunction;
	}
	
	/** Create the agent on the given platform. */
	public final static Agent create(final Platform platform, final AgentSpecification specification) {
		return platform.addAgent(specification);
	}
	
	/** Create the agent on a hidden new single threaded platform. */
	public final static Agent create(final AgentSpecification specification) {
		final Platform platform = new Platform();
		final Agent result = platform.addAgent(specification);
		result.addDeathListener(id -> platform.halt());
		return result;
	}
	
	/** Create an agent which will only execute a deliberation cycle if the returned runnable 
	 * is executed. The platform is only used for inter-agent messaging, not for execution. */
	public final static Entry<Agent, Runnable> createAgentRunnable(final Platform platform, final AgentSpecification specification) {
		return platform.makeAgentRunnable(specification);
	}

	/** Add a listener that listens for the death of this agent. */
	public final void addDeathListener(final Consumer<AgentID> listener){
		this.state.addDeathListener(listener);
	}
	
	/** Remove a listener that listens for the death of this agent. */
	public final void removeDeathListener(final Consumer<AgentID> listener){
		this.state.removeDeathListener(listener);
	}
	
	/** Put an external event in this agent. Will be processed the next deliberation cycle. */
	public final void add(final Trigger externalTrigger){
		this.state.addExternalTrigger(externalTrigger);
	}
	
	/** Kills the agent. Will immediately notify the death listeners. */
	public final void kill() {
		this.killFunction.run();
	}
	
	/** Obtain the id of the agent that is exposed through this interface. */
	public final AgentID getAgentID(){ 
		return this.state.getAgentID(); 
	}
	
	/** Whether the agent should be or is executing more deliberation cycles. */
	public final boolean isSleeping() {
		return this.state.isSleeping();
	}
	
	/** Whether the agent will not execute deliberation cycles anymore. */
	public final boolean isDead() {
		return this.state.isDead();
	}
}
