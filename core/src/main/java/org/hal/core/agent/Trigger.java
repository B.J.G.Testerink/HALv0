package org.hal.core.agent;

/**
 * A trigger is anything that can trigger a plan for an agent (as specified by 
 * the plan's scheme).  
 */
public interface Trigger {} 