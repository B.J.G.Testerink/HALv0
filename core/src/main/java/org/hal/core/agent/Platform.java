package org.hal.core.agent;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * The platform class maintains the currently alive agents. This allows 
 * an administrator to kill agents that they didn't create themselves. 
 * The platform also enables one agent to send a trigger to another 
 * agent. Finally, we use a standard executor service for running 
 * the agents. The platform can halt this executor, which will 
 * also call the kill method for all the agents.
 */
public final class Platform {
	/** The service upon which we execute the agents. */
	private final ExecutorService executor;
	/** The currently alive agents. */
	private final Map<AgentID, AgentState> agents;
	
	/** Initiate the platform with a fixed threadpool of the specified amount. */
	public Platform(final int nrOfThreads) {
		this.executor = Executors.newFixedThreadPool(nrOfThreads);
		this.agents = new HashMap<>();
	}

	/** Initiate the platform with a fixed threadpool of 1. */
	public Platform() {
		this.executor = Executors.newFixedThreadPool(1);
		this.agents = new HashMap<>();
	}
	
	/** Send a trigger from one agent to the other. */
	private final void send(final AgentID agentID, final Trigger trigger) {
		if(agentID == null || trigger == null) return;
		final AgentState state;
		synchronized(this.agents) {
			state = this.agents.get(agentID);
		}
		if(state != null) 
			state.addExternalTrigger(trigger);
	}
	
	/** Add create an agent form its specification. */
	final Agent addAgent(final AgentSpecification specification) {
		final AgentState state = specification.makeState(this.executor::execute, this::send);
		synchronized(this.agents){
			this.agents.put(state.getAgentID(), state); 
		}
		this.executor.execute(state);
		return new Agent(state, () -> killAgent(state.getAgentID()));
	}
	
	/** Create and return an agent and a runnable that, if executed, runs a single deliberation cycle. */
	final Entry<Agent, Runnable> makeAgentRunnable(final AgentSpecification specification){
		final AgentState state = specification.makeState(s -> {}, this::send);
		synchronized(this.agents){
			this.agents.put(state.getAgentID(), state); 
		} 
		final Agent agent = new Agent(state, () -> killAgent(state.getAgentID()));
		return new AbstractMap.SimpleEntry<Agent, Runnable>(agent, () -> state.run());
	}
	
	/** Kills an agent, will notify its death listeners. */
	private final void killAgent(final AgentID agentID){  
		if(agentID != null) {
			final AgentState state;
			synchronized(this.agents) {
				state = agents.remove(agentID);
			}
			if(state != null)
				state.kill();
		}
	}
	
	/** Halt the executor service and kill all agents. */
	public final void halt() { 
		if(!this.executor.isShutdown()) {
			synchronized(this.agents) {
				for(AgentState state : agents.values())
					state.kill();
				this.agents.clear();
			}
			this.executor.shutdown();
		}
	}
}
